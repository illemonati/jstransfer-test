
let predict = async () => {
    const model = await tf.loadLayersModel('model.json');
    let a = parseInt(document.querySelector("#a").value);
    let b = parseInt(document.querySelector("#b").value);
    let res = null;
    if (!((a == 0) || (a == 1))) {
        console.log(a);
        res = "a is not 0 or 1"
    } else if (!((b == 0) || (b == 1))) {
        res = "b is not 0 or 1"
    } else {
        x = tf.tensor([[a, b]]);
        res = model.predict(x);
        res = res.dataSync();
        res = Math.round(res[0])
    }
    document.querySelector("#result").innerHTML = res;
};

