import keras
import tensorflow.keras as keras
import tensorflowjs as tfjs


def build_model():
    model = keras.models.Sequential([
        keras.layers.Dense(2, input_shape=[2], activation='relu'),
        keras.layers.Dense(4, activation='relu'),
        keras.layers.Dense(1, activation='sigmoid')
    ])
    return model


x = [[0, 0], [0, 1], [1, 0], [1, 1]]
y = [(a ^ b) for (a, b) in x]

model = build_model()
model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])

model.fit(x, y, epochs=750, verbose=1)

print("results:")
for (a, b) in x:
    print(round(model.predict([(a, b)])[0][0]))
model.save('model.hdf5')
tfjs.converters.save_keras_model(model, 'js')
